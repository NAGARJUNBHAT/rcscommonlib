package Media

data class ContentMessage(
    val contentInfo: ContentInfo
)