package SuggestedActions.CalenderEvent

data class ContentMessage(
    val suggestions: List<Suggestion>,
    val text: String
)