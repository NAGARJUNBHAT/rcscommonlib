package SuggestedActions.CalenderEvent

data class Action(
    val createCalendarEventAction: CreateCalendarEventAction,
    val fallbackUrl: String,
    val postbackData: String,
    val text: String
)