package SuggestedActions.CalenderEvent

data class CreateCalendarEventAction(
    val description: String,
    val endTime: String,
    val startTime: String,
    val title: String
)