package SuggestedActions.DialNumber

data class DialAction(
    val phoneNumber: String
)