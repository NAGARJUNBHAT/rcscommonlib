package SuggestedActions.DialNumber

data class ContentMessage(
    val suggestions: List<Suggestion>,
    val text: String
)