package SuggestedActions.DialNumber

data class Action(
    val dialAction: DialAction,
    val fallbackUrl: String,
    val postbackData: String,
    val text: String
)