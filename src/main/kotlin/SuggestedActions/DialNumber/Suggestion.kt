package SuggestedActions.DialNumber

data class Suggestion(
    val action: Action
)