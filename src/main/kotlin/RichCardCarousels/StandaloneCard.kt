package RichCardCarousels

data class StandaloneCard(
    val cardContent: CardContent,
    val cardOrientation: String,
    val thumbnailImageAlignment: String
)