package RichCardCarousels

data class Media(
    val contentInfo: ContentInfo,
    val height: String
)