package RichCardCarousels

data class CardContent(
    val description: String,
    val media: Media,
    val suggestions: List<Suggestion>,
    val title: String
)