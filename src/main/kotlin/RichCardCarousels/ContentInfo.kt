package RichCardCarousels

data class ContentInfo(
    val fileUrl: String,
    val forceRefresh: String
)