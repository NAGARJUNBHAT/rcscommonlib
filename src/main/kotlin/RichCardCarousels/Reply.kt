package RichCardCarousels

data class Reply(
    val postbackData: String,
    val text: String
)