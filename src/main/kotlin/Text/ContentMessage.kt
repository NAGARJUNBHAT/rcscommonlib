package Text

data class ContentMessage(
    val text: String
)