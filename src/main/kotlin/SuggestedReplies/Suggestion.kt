package SuggestedReplies

data class Suggestion(
    val reply: Reply
)