package SuggestedReplies

data class Reply(
    val postbackData: String,
    val text: String
)