package SuggestedReplies

data class ContentMessage(
    val suggestions: List<Suggestion>,
    val text: String
)