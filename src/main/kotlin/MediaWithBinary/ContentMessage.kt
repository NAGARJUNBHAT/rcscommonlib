package MediaWithBinary

data class ContentMessage(
    val fileName: String
)